// When we click on a menu item

const menuFunctions = {
    openDropDownMenu: function (e) {
        let dropdown = e.target;
        dropdown.classList.add("show");
        let dropdowns = document.querySelectorAll(".menu-dropdown.show");
        // console.log(dropdowns);
        for(let i = 0; i < dropdowns.length; i++ ){
            let openDropdown = dropdowns[i];
            if(! isAncestorOf(openDropdown, dropdown))
                openDropdown.classList.remove("show");

        }
        return false;
    },
    closeAllDropDowns: function (event) {
        if (!(event.target.matches && event.target.matches(".menu-dropdown"))) {
            let dropdowns = document.querySelectorAll(".menu-dropdown.show");
            for (let i = 0; i < dropdowns.length; i++) {
                let openDropdown = dropdowns[i];
                openDropdown.classList.remove("show");
            }
        }
    }
}
const eventData = {
    // "event1":{
    //     "selector1":[
    //         {
    //             "func":function,
    //             "otherArgs":["some", "args"]
    //         }
    //     ]
    // }

    // bubbling will depend upon the last function call
    click: {
        ".menu-dropdown": {
            func: menuFunctions.openDropDownMenu
        },
        "window": {
            func: menuFunctions.closeAllDropDowns
        },
        ".menu-item": {
            func: menuFunctions.closeAllDropDowns
        },
        "#add-cube": [
            {
                func: a3s.createMesh.bind(a3s),
                includeEvent:false,
                otherArgs: ["Box", {}],
            }
        ],
        "#add-sphere":{
            func: a3s.createMesh.bind(a3s),
            includeEvent:false,
            otherArgs: ["Sphere", {}],
        },
        "#renderCanvas": [
            {
                func: a3s.pickMeshOnClick.bind(a3s)
            },
            {
                func: function() { console.log("trying to bubble"); return true; }
            }
        ]
    },
    resize: {
        "window":{
            func: a3s.resizeCanvas
        }
    }
}

function ancestorsOf(elem, arr = []) {
    if (!elem) return;
    arr.push(elem);
    ancestorsOf(elem.parentNode, arr);
    return arr;
}
function isAncestorOf(parent, elem) {
    
    const anc = ancestorsOf(elem);
    
    for (let i = 0; i < anc.length; i++) {
        if (parent === anc[i]) return true;
    }
    return false;
}

function callFunctionArray(functionArray, e) {
    if (!Array.isArray(functionArray)) {
        functionArray = [functionArray];
    }
    let res;
    for (let index = 0; index < functionArray.length; index++) {
        const functionObj = functionArray[index];
        
        if (!functionObj.otherArgs)
            functionObj.otherArgs = [];
        if (!Array.isArray(functionObj.otherArgs)) {
            funtionObj.otherArgs = [functionObj.otherArgs];
        }
        if(functionObj.includeEvent === undefined)
            functionObj.includeEvent = true;

        if(functionObj.includeEvent)
            res = functionObj.func(e, ...functionObj.otherArgs);
        else 
            res = functionObj.func(...functionObj.otherArgs);
    }
    return res;
}

function handleEvent(e) {
    const eventName = e.type;
    const eventObj = eventData[eventName];
    console.log(eventName, e.target);


    if (eventObj) {
        let bubble = true;
        for (let selector in eventObj) {
            
            let functionArray = eventObj[selector];
            // console.log(e.target, selector, e.target.matches(selector));
            if (e.target.matches && e.target.matches(selector)) {
                bubble = callFunctionArray(functionArray, e);
            }
            else if (selector === "document" && e.target === document) {
                bubble = callFunctionArray(functionArray, e);
            }
            else if (selector === "window" && e.target === window) {
                bubble = callFunctionArray(functionArray, e);
            }
        }
        if (e.target.parentNode && bubble) {
            let new_event = new e.constructor(e.type, e);
            e.target.parentNode.dispatchEvent(new_event);
        } else if(e.target === document){
            let new_event = new e.constructor(e.type, e);
            window.dispatchEvent(new_event);
        }
    }
}
document.parentElement = window;
window.addEventListener("click", handleEvent);
window.addEventListener("resize", handleEvent);