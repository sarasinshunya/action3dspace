# Methods list in this repo (**Tentative**):

class/module > property/method > description

## action3dspace

- init(canvasElement)
  - Initates the scene, creates the default mesh, light and camera to start with.
  - Creates a mesh editor object for editing meshes.
- createMesh(meshName, meshId, options)
  - creates a mesh in the scene

## meshEditor

- editMesh(meshObj)
  - Takes a mesh object as input which is to be edited.
  - renders vertices, edges, faces (depeding upon the mode selected) which can then be moved to alter the mesh geometry.
- updateVertexData(vertexArray)
  - Takes an array consisting of vertex Array.
  - Takes a mesh object whose vertex data is to be updated
  - updates vertex positions.
- createVertex(x, y, z)
- createEdge(vertex1, vertex2)
  - creates an newly created `edge` between `vertex1` and `vertex2`
- createFace(...vertices)
## vector

> Represents a vector $x\hat{i} + y\hat{j} + z\hat{k}$

- x
  - magnitude in the x-direction
- y
  - magnitude in the y-direction
- z
  - magnitude in the z-direction
- add(vect)
- subtract(vect)
- multiply(num)
- magnitude()
- parent
  - parent object

## vertex

> Represents a 3d space coordinate (x, y, z)
- constructor(x, y, z)
- x
- y
- z
- vectorTo(vert)
  - Returns a vector from `this` vertex to `vert` vertex
- move(vect)
  - moves `this` vertex by `vect` vector
  
## edge

> Represents a directed edge containing two points in 3d-space coordinate 
- constructor(fromVertex, toVertex)
- split(fraction)
  - splits an edge between vertex1 and vertex2 by creating a new vertex (say `vertex3`) on the edge.
  - The `vertex3` is calculated using 
    ```js
    let vctr = vertex1.vectorTo(vertex2).multiply(fraction);
    let vertex3 = vertex.move(vctr);
    ```
  - deletes this edge
  - returns two newly created edges

## face

> represents a face containing three or more vertices for creating a polygon.

