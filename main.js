
class Action3dspace {
    constructor(canvas){
        this.canvas = canvas;
        this.objects = {};
        this.selectedObjectIds = [];
    }
    init(){
        this.engine = new BABYLON.Engine(this.canvas, true);
        this.scene = new BABYLON.Scene(this.engine);

        let thisObj = this;

        this.defaultCamera = new BABYLON.ArcRotateCamera("camera", -Math.PI / 2, Math.PI / 2.5, 15, new BABYLON.Vector3(0, 0, 0), this.scene);
        this.defaultCamera.attachControl(this.canvas, true);

        const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 1), this.scene);

        const ground = this.createMesh("Ground", {width:10, height:10}, this.scene);
        
        this.engine.runRenderLoop(function() {
            thisObj.scene.render();
        })
        // this.scene.debugLayer.show();
    }
    createArcRotateCamera(alpha, beta, radius, target, scene, id){

    }
    createMesh(meshName, options, scene, id) {
        console.log(arguments);
        if(!scene) scene = this.scene;
        if(!id) id = this.nextIdOf(meshName);
        if(!options) options = {};
        const mesh = BABYLON.MeshBuilder["Create"+meshName](id, options, scene);
        
        this.objects[id] = mesh;
    }
    nextIdOf(meshName){
        for(let i=1;;i++){
            if(! this.objects[meshName+i])
                return meshName+i;
        }
    }
    resizeCanvas() {
        let rect = document.getElementById("canvaszone").getBoundingClientRect();
        a3s.canvas.width = rect.width;
        a3s.canvas.height = rect.height;
    }
    pickMeshOnClick(e) {
        console.log(this);
        let rect = this.canvas.getBoundingClientRect();
        let x = e.clientX - rect.left ;
        let y = e.clientY - rect.top ;

        let result = this.scene.pick(x, y);
        if(result.pickedMesh)
            this.selectMesh(result.pickedMesh.id);
        else 
            this.deselectAllMeshes();
    }
    selectMesh(id, includeLast = false){
        if(! includeLast){
            this.deselectAllMeshes();
        }
        let index = this.selectedObjectIds.indexOf(id);
        if(index >= 0){
            return; // Mesh already selected
        }
        const mesh = this.objects[id];
        mesh.enableEdgesRendering();
        mesh.edgesWidth = 8.0;
        mesh.edgesColor = new BABYLON.Color4(1, 0, 0, 1);
        this.selectedObjectIds.push(id);
    }
    deselectAllMeshes(){
        while(this.selectedObjectIds.length){
            this.deselectMesh(this.selectedObjectIds[0]);
        }
    }
    deselectMesh(id){
        let index = this.selectedObjectIds.indexOf(id);
        const mesh = this.objects[id];
        mesh.disableEdgesRendering();
        this.selectedObjectIds.splice(index, 1);
    }
}


const canvas = document.getElementById("renderCanvas"); // Get the canvas element
const a3s = new Action3dspace(canvas);

a3s.init();